package ru.t1.vlvov.tm.exception.field;

import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.exception.AbstractException;

public abstract class AbstractFieldNotFoundException extends AbstractException {

    public AbstractFieldNotFoundException() {
        super();
    }

    public AbstractFieldNotFoundException(@Nullable final String message) {
        super(message);
    }

    public AbstractFieldNotFoundException(@Nullable final String message, @Nullable final Throwable cause) {
        super(message, cause);
    }

    public AbstractFieldNotFoundException(@Nullable final Throwable cause) {
        super(cause);
    }

    protected AbstractFieldNotFoundException(
            @Nullable final String message,
            @Nullable final Throwable cause,
            @Nullable final boolean enableSuppression,
            @Nullable final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
