package ru.t1.vlvov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import lombok.SneakyThrows;
import org.bson.Document;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedHashMap;
import java.util.Map;

public class LoggerService {

    private final ObjectMapper mapper = new ObjectMapper();

    private final MongoClient mongoClient = new MongoClient("localhost", 27017);

    MongoDatabase mongoDatabase = mongoClient.getDatabase("example");

    @SneakyThrows
    public void log(@NotNull final String json) {
        final Map<String, Object> event = mapper.readValue(json, LinkedHashMap.class);
        final String collectionName = event.get("table").toString();
        if (mongoDatabase.getCollection(collectionName) == null) mongoDatabase.createCollection(collectionName);
        final MongoCollection<Document> collection = mongoDatabase.getCollection(collectionName);
        collection.insertOne(new Document(event));
    }

}
