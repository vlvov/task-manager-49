package ru.t1.vlvov.tm.api.service.dto;

import ru.t1.vlvov.tm.api.repository.dto.IUserOwnedRepositoryDTO;
import ru.t1.vlvov.tm.dto.model.AbstractUserOwnedModelDTO;

public interface IUserOwnedServiceDTO<M extends AbstractUserOwnedModelDTO> extends IServiceDTO<M>, IUserOwnedRepositoryDTO<M> {
}
