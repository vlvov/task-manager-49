package ru.t1.vlvov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.api.repository.dto.IUserRepositoryDTO;
import ru.t1.vlvov.tm.dto.model.UserDTO;

import javax.persistence.EntityManager;
import java.util.List;

public final class UserRepositoryDTO extends AbstractRepositoryDTO<UserDTO> implements IUserRepositoryDTO {

    public UserRepositoryDTO(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void clear() {
        @NotNull final String jpql = "DELETE FROM UserDTO";
        entityManager.createQuery(jpql).executeUpdate();
    }

    @Override
    @Nullable
    public List<UserDTO> findAll() {
        @NotNull final String jpql = "SELECT m FROM UserDTO m";
        return entityManager.createQuery(jpql, UserDTO.class)
                .setHint("org.hibernate.cacheable", true)
                .getResultList();
    }

    @Override
    @Nullable
    public UserDTO findOneById(@NotNull final String id) {
        return entityManager.find(UserDTO.class, id);
    }

    @Override
    public void removeById(@NotNull final String id) {
        @Nullable final UserDTO model = findOneById(id);
        if (model != null) entityManager.remove(model);
    }

    @Override
    public boolean existsById(@NotNull String id) {
        return findOneById(id) != null;
    }

    @Override
    @Nullable
    public UserDTO findByLogin(@NotNull String login) {
        @NotNull final String jpql = "SELECT m FROM UserDTO m WHERE login = :login";
        return entityManager.createQuery(jpql, UserDTO.class)
                .setHint("org.hibernate.cacheable", true)
                .setParameter("login", login)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    @Nullable
    public UserDTO findByEmail(@NotNull String email) {
        @NotNull final String jpql = "SELECT m FROM UserDTO m WHERE email = :email";
        return entityManager.createQuery(jpql, UserDTO.class)
                .setHint("org.hibernate.cacheable", true)
                .setParameter("email", email)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

}
