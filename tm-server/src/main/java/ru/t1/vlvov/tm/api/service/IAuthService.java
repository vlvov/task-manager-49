package ru.t1.vlvov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.dto.model.SessionDTO;
import ru.t1.vlvov.tm.dto.model.UserDTO;

public interface IAuthService {

    @Nullable
    UserDTO registry(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    String login(@Nullable final String login, @Nullable final String password);

    @NotNull
    SessionDTO validateToken(@Nullable final String token);

    @NotNull
    void logout(@Nullable final String token);

}
