package ru.t1.vlvov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.t1.vlvov.tm.enumerated.Sort;

import java.util.List;

public interface IUserOwnedRepositoryDTO<M extends AbstractUserOwnedModelDTO> extends IRepositoryDTO<M> {

    void add(@NotNull final String userId, @NotNull final M model);

    void update(@NotNull final String userId, @NotNull final M model);

    void clear(@NotNull final String userId);

    @Nullable
    List<M> findAll(@NotNull final String userId);

    @Nullable
    M findOneById(@NotNull final String userId, @NotNull final String id);

    void remove(@NotNull final String userId, @NotNull final M model);

    void removeById(@NotNull final String userId, @NotNull final String id);

    boolean existsById(@NotNull final String userId, @NotNull final String id);

    @Nullable
    List<M> findAll(@NotNull final String userId, @NotNull final Sort sort);

}
