package ru.t1.vlvov.tm.api.service.dto;

import ru.t1.vlvov.tm.api.repository.dto.IRepositoryDTO;
import ru.t1.vlvov.tm.dto.model.AbstractModelDTO;

public interface IServiceDTO<M extends AbstractModelDTO> extends IRepositoryDTO<M> {
}
