package ru.t1.vlvov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.api.repository.dto.IProjectRepositoryDTO;
import ru.t1.vlvov.tm.dto.model.ProjectDTO;
import ru.t1.vlvov.tm.enumerated.Sort;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public final class ProjectRepositoryDTO extends AbstractUserOwnedRepositoryDTO<ProjectDTO> implements IProjectRepositoryDTO {

    public ProjectRepositoryDTO(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void clear() {
        @NotNull final String jpql = "DELETE FROM ProjectDTO";
        entityManager.createQuery(jpql).executeUpdate();
    }

    @Override
    @Nullable
    public List<ProjectDTO> findAll() {
        @NotNull final String jpql = "SELECT m FROM ProjectDTO m";
        return entityManager.createQuery(jpql, ProjectDTO.class).getResultList();
    }

    @Override
    @Nullable
    public ProjectDTO findOneById(@NotNull final String id) {
        return entityManager.find(ProjectDTO.class, id);
    }

    @Override
    public void removeById(@NotNull final String id) {
        @Nullable final ProjectDTO model = findOneById(id);
        if (model != null) entityManager.remove(model);
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return findOneById(id) != null;
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final String jpql = "DELETE FROM ProjectDTO m WHERE m.userId = :userId";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    @Nullable
    public List<ProjectDTO> findAll(@NotNull final String userId) {
        @NotNull final String jpql = "SELECT m FROM ProjectDTO m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, ProjectDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    @Nullable
    public ProjectDTO findOneById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String jpql = "SELECT m FROM ProjectDTO m WHERE m.userId = :userId AND m.id = :id";
        return entityManager.createQuery(jpql, ProjectDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final ProjectDTO model = findOneById(userId, id);
        if (model != null) entityManager.remove(model);
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return findOneById(userId, id) != null;
    }

    @Override
    @Nullable
    public List<ProjectDTO> findAll(@NotNull final String userId, @NotNull final Sort sort) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<ProjectDTO> criteriaQuery = criteriaBuilder.createQuery(ProjectDTO.class);
        Root<ProjectDTO> project = criteriaQuery.from(ProjectDTO.class);
        criteriaQuery.select(project)
                .where(criteriaBuilder.equal(project.get("userId"), userId))
                .orderBy(criteriaBuilder.asc(project.get(Sort.getOrderByField(sort))));
        TypedQuery<ProjectDTO> typedQuery = entityManager.createQuery(criteriaQuery);
        return typedQuery.getResultList();
    }

}
