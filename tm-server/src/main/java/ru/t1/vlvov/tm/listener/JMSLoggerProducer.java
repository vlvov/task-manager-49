package ru.t1.vlvov.tm.listener;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.BrokerService;
import org.jetbrains.annotations.NotNull;
import ru.t1.vlvov.tm.log.OperationEvent;

import javax.jms.*;
import javax.persistence.Table;
import java.lang.annotation.Annotation;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class JMSLoggerProducer {

    private static final String URL = ActiveMQConnection.DEFAULT_BROKER_URL;

    private static final String QUEUE = "LOGGER";

    private final BrokerService brokerService = new BrokerService();

    private final ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(URL);

    private final Connection connection;

    private final Session session;

    private final Queue destination;

    private final MessageProducer messageProducer;

    private final ObjectWriter objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();

    private final ExecutorService executorService = Executors.newCachedThreadPool();

    @SneakyThrows
    public JMSLoggerProducer() {
        connection = connectionFactory.createConnection();
        connection.start();
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        destination = session.createQueue(QUEUE);
        messageProducer = session.createProducer(destination);
    }

    @SneakyThrows
    public void send(@NotNull final  String text) {
        final TextMessage message = session.createTextMessage(text);
        messageProducer.send(message);
    }

    public void send(@NotNull final  OperationEvent event) {
        executorService.submit(() -> sync(event));
    }

    @SneakyThrows
    private void sync(@NotNull final  OperationEvent event) {
        @NotNull final  Class entityClass = event.getEntity().getClass();
        if (entityClass.isAnnotationPresent(Table.class)) {
            final Annotation annotation = entityClass.getAnnotation(Table.class);
            final Table table = (Table) annotation;
            event.setTable(table.name());
        }
        @NotNull final  String json = objectWriter.writeValueAsString(event);
        send(json);
    }

}
