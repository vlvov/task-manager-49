package ru.t1.vlvov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.api.service.IConnectionService;
import ru.t1.vlvov.tm.api.service.dto.IProjectServiceDTO;
import ru.t1.vlvov.tm.api.service.dto.IProjectTaskServiceDTO;
import ru.t1.vlvov.tm.api.service.dto.ITaskServiceDTO;
import ru.t1.vlvov.tm.dto.model.ProjectDTO;
import ru.t1.vlvov.tm.dto.model.TaskDTO;
import ru.t1.vlvov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.vlvov.tm.exception.entity.TaskNotFoundException;
import ru.t1.vlvov.tm.exception.field.IdEmptyException;

import java.util.List;

public final class ProjectTaskServiceDTO implements IProjectTaskServiceDTO {

    @NotNull
    protected final IConnectionService connectionService;

    public ProjectTaskServiceDTO(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    private ITaskServiceDTO getTaskService() {
        return new TaskServiceDTO(connectionService);
    }

    @NotNull
    private IProjectServiceDTO getProjectService() {
        return new ProjectServiceDTO(connectionService);
    }

    @Override
    public void bindTaskToProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new IdEmptyException();
        if (!getProjectService().existsById(userId, projectId)) throw new ProjectNotFoundException();
        @NotNull final ITaskServiceDTO taskServiceDTO = getTaskService();
        TaskDTO task = taskServiceDTO.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
        taskServiceDTO.update(task);
    }

    @Override
    public void removeProjectById(@Nullable String userId, @Nullable String projectId) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();
        @NotNull final IProjectServiceDTO projectServiceDTO = getProjectService();
        @Nullable final ProjectDTO projectDTO = projectServiceDTO.findOneById(userId, projectId);
        if (projectDTO == null) throw new ProjectNotFoundException();
        @NotNull final ITaskServiceDTO taskServiceDTO = getTaskService();
        List<TaskDTO> tasks = taskServiceDTO.findAllByProjectId(projectId);
        if (tasks != null)
            for (final TaskDTO task : tasks) {
                taskServiceDTO.remove(task);
            }
        projectServiceDTO.remove(projectDTO);
    }

    @Override
    public void unbindTaskFromProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new IdEmptyException();
        if (!getProjectService().existsById(userId, projectId)) throw new ProjectNotFoundException();
        @NotNull final ITaskServiceDTO taskServiceDTO = getTaskService();
        TaskDTO task = taskServiceDTO.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        @Nullable final String taskProjectId = task.getProjectId();
        if (taskProjectId == null || taskProjectId.isEmpty() || !taskProjectId.equals(projectId))
            throw new TaskNotFoundException();
        task.setProjectId(null);
        taskServiceDTO.update(task);
    }

}
