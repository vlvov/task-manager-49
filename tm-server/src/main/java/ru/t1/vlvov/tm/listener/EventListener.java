package ru.t1.vlvov.tm.listener;

import org.hibernate.event.spi.*;
import org.hibernate.persister.entity.EntityPersister;
import org.jetbrains.annotations.NotNull;
import ru.t1.vlvov.tm.log.OperationEvent;
import ru.t1.vlvov.tm.log.OperationType;

public class EventListener implements PostInsertEventListener, PostDeleteEventListener, PostUpdateEventListener {

    private final JMSLoggerProducer jmsLoggerProducer;

    public EventListener(JMSLoggerProducer jmsLoggerProducer) {
        this.jmsLoggerProducer = jmsLoggerProducer;
    }

    @Override
    public void onPostDelete(@NotNull final PostDeleteEvent postDeleteEvent) {
        log(new OperationEvent(postDeleteEvent.getEntity(), OperationType.DELETE));
    }

    @Override
    public void onPostInsert(@NotNull final PostInsertEvent postInsertEvent) {
        log(new OperationEvent(postInsertEvent.getEntity(), OperationType.INSERT));
    }

    @Override
    public void onPostUpdate(@NotNull final PostUpdateEvent postUpdateEvent) {
        log(new OperationEvent(postUpdateEvent.getEntity(), OperationType.UPDATE));
    }

    @Override
    public boolean requiresPostCommitHanding(@NotNull final EntityPersister entityPersister) {
        return false;
    }

    private void log(@NotNull final OperationEvent event) {
        jmsLoggerProducer.send(event);
    }

}
