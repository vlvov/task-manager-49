package ru.t1.vlvov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.api.repository.model.ISessionRepository;
import ru.t1.vlvov.tm.enumerated.Sort;
import ru.t1.vlvov.tm.model.Session;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void clear() {
        @NotNull final String jpql = "DELETE FROM Session m";
        entityManager.createQuery(jpql).executeUpdate();
    }

    @Override
    @Nullable
    public List<Session> findAll() {
        @NotNull final String jpql = "SELECT m FROM Session m";
        return entityManager.createQuery(jpql, Session.class).getResultList();
    }

    @Override
    @Nullable
    public Session findOneById(@NotNull final String id) {
        return entityManager.find(Session.class, id);
    }

    @Override
    public void removeById(@NotNull final String id) {
        @Nullable final Session model = findOneById(id);
        if (model != null) entityManager.remove(model);
    }

    @Override
    public boolean existsById(@NotNull String id) {
        return findOneById(id) != null;
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final String jpql = "DELETE FROM Session m WHERE user.id = :userId";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    @Nullable
    public List<Session> findAll(@NotNull final String userId) {
        @NotNull final String jpql = "SELECT m FROM Session m WHERE user.id = :userId";
        return entityManager.createQuery(jpql, Session.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    @Nullable
    public Session findOneById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String jpql = "SELECT m FROM Session m WHERE user.id = :userId AND id = :id";
        return entityManager.createQuery(jpql, Session.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Session model = findOneById(userId, id);
        if (model != null) entityManager.remove(model);
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return findOneById(userId, id) != null;
    }

    @Override
    @Nullable
    public List<Session> findAll(@NotNull final String userId, @NotNull final Sort sort) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Session> criteriaQuery = criteriaBuilder.createQuery(Session.class);
        Root<Session> session = criteriaQuery.from(Session.class);
        criteriaQuery.select(session)
                .where(criteriaBuilder.equal(session.get("userId"), userId))
                .orderBy(criteriaBuilder.asc(session.get(Sort.getOrderByField(sort))));
        TypedQuery<Session> typedQuery = entityManager.createQuery(criteriaQuery);
        return typedQuery.getResultList();
    }

}
