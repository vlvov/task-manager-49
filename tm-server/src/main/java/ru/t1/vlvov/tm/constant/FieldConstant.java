package ru.t1.vlvov.tm.constant;

import org.jetbrains.annotations.NotNull;

public interface FieldConstant {

    @NotNull
    static final String ID = "ID";

    @NotNull
    static final String NAME = "NAME";

    @NotNull
    static final String DESCRIPTION = "DESCRIPTION";

    @NotNull
    static final String USER_ID = "USER_ID";

    @NotNull
    static final String STATUS = "STATUS";

    @NotNull
    static final String CREATED = "CREATED";

    @NotNull
    static final String ROLE = "ROLE";

    @NotNull
    static final String PROJECT_ID = "PROJECT_ID";

    @NotNull
    static final String LOGIN = "LOGIN";

    @NotNull
    static final String PASSWORD = "PASSWORD";

    @NotNull
    static final String LOCKED = "LOCKED";

    @NotNull
    static final String FIRST_NAME = "FIRST_NAME";

    @NotNull
    static final String LAST_NAME = "LAST_NAME";

    @NotNull
    static final String MIDDLE_NAME = "MIDDLE_NAME";

    @NotNull
    static final String EMAIL = "EMAIL";

}
