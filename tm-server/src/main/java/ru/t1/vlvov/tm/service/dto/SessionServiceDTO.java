package ru.t1.vlvov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.vlvov.tm.api.repository.dto.ISessionRepositoryDTO;
import ru.t1.vlvov.tm.api.service.IConnectionService;
import ru.t1.vlvov.tm.api.service.dto.ISessionServiceDTO;
import ru.t1.vlvov.tm.dto.model.SessionDTO;
import ru.t1.vlvov.tm.repository.dto.SessionRepositoryDTO;

import javax.persistence.EntityManager;

public final class SessionServiceDTO extends AbstractUserOwnedServiceDTO<SessionDTO, ISessionRepositoryDTO> implements ISessionServiceDTO {

    public SessionServiceDTO(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    protected @NotNull ISessionRepositoryDTO getRepository(@NotNull final EntityManager entityManager) {
        return new SessionRepositoryDTO(entityManager);
    }

}
