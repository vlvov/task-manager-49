package ru.t1.vlvov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.dto.model.TaskDTO;
import ru.t1.vlvov.tm.dto.request.TaskGetByIdRequest;
import ru.t1.vlvov.tm.util.TerminalUtil;

public final class TaskShowByIdCommand extends AbstractTaskCommand {

    @NotNull
    private final String DESCRIPTION = "Show task by Id.";

    @NotNull
    private final String NAME = "task-show-by-id";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @Nullable final TaskGetByIdRequest request = new TaskGetByIdRequest(getToken());
        request.setTaskId(id);
        @Nullable final TaskDTO task = getTaskEndpoint().getTaskById(request).getTask();
        showTask(task);
    }

}