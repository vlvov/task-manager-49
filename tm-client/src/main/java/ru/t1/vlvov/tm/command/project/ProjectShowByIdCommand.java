package ru.t1.vlvov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.dto.model.ProjectDTO;
import ru.t1.vlvov.tm.dto.request.ProjectGetByIdRequest;
import ru.t1.vlvov.tm.util.TerminalUtil;

public final class ProjectShowByIdCommand extends AbstractProjectCommand {

    @NotNull
    private final String DESCRIPTION = "Show project by Id.";

    @NotNull
    private final String NAME = "project-show-by-id";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @Nullable final ProjectGetByIdRequest request = new ProjectGetByIdRequest(getToken());
        request.setProjectId(id);
        @Nullable final ProjectDTO project = getProjectEndpoint().getById(request).getProject();
        showProject(project);
    }

}