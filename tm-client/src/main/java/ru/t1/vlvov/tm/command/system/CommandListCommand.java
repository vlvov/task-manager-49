package ru.t1.vlvov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.api.model.ICommand;
import ru.t1.vlvov.tm.command.AbstractCommand;

import java.util.Collection;

public final class CommandListCommand extends AbstractSystemCommand {

    @NotNull
    private final String ARGUMENT = "-cmd";

    @NotNull
    private final String DESCRIPTION = "Show commands.";

    @NotNull
    private final String NAME = "commands";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        @NotNull final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        System.out.println("[COMMANDS]");
        for (@NotNull final ICommand command : commands) {
            @Nullable final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

}
